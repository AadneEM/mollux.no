## This is the source code for mollux.no

For this project to work you need to run migrations and install the requirements by running the command `pip install -r requirements.txt`.
It is recommended have this running in a virtual environment.
