from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib import messages
from django.conf import settings
from django.db.models import Q

from .models import Posts
from .models import SidebarElements
from .forms import PostForm, MessageForm

def index(request):
    post_list = Posts.objects.all().order_by('-created_at')

    query = request.GET.get("search")
    if query:
        post_list = post_list.filter(
            Q(title__icontains=query) |
            Q(summary__icontains=query) |
            Q(body__icontains=query)
        )

    paginator = Paginator(post_list, 10)

    messageForm = MessageForm(request.POST or None)
    messageFormHandeling(messageForm, request)

    page = request.GET.get('page')
    posts = paginator.get_page(page)
    
    context = {
        'title': 'Latest Posts',
        'posts': posts,
        'messageForm': messageForm,
        'num_pages': range(posts.paginator.num_pages)
    }
    
    return render(request, 'blog/index.html', context)

def details(request, id):
    post = Posts.objects.get(id=id)

    messageForm = MessageForm(request.POST or None)
    messageFormHandeling(messageForm, request)

    context = {
        'post': post,
        'messageForm': messageForm,
    }

    return render(request, 'blog/details.html', context)

def post(request):
    if not request.user.is_authenticated:
        return redirect('%s?next=%s' % (settings.LOGIN_URL, request.path))

    form = PostForm(request.POST or None, request.FILES or None)

    messageForm = MessageForm(request.POST or None)
    messageFormHandeling(messageForm, request)

    if form.is_valid():
        print ("Is valid :D")
        instance = form.save(commit=False)
        instance.save()
    else:
        print(form.errors)
        print ("Is not valid :(")
        messages.error(request, "Error")

    context = {
        "form": form,
        'messageForm': messageForm,
    }

    return render(request, 'blog/post.html', context)

def about(request):
    messageForm = MessageForm(request.POST or None)
    messageFormHandeling(messageForm, request)

    context = {
        'messageForm': messageForm,
    }

    return render(request, 'blog/about.html')

def login(request):
    messageForm = MessageForm(request.POST or None)
    messageFormHandeling(messageForm, request)

    context = {
        'messageForm': messageForm,
    }

    return render(request, 'blog/login.html')

def gallery(request):
    post_list = Posts.objects.all().order_by('-created_at')

    query = request.GET.get("search")
    if query:
        post_list = post_list.filter(
            Q(title__icontains=query) 
        )

    messageForm = MessageForm(request.POST or None)
    messageFormHandeling(messageForm, request)

    context = {
        'posts': post_list,
        'messageForm': messageForm,
    }

    return render(request, 'blog/gallery.html', context)

def messageFormHandeling(messageForm, request):
    if messageForm.is_valid():
        instance = messageForm.save(commit=False)
        instance.save()
    else:
        print(messageForm.errors)
        print ("Is not valid :(")
        messages.error(request, "Error")
