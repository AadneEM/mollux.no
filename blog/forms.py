from django import forms
from django.utils.translation import gettext_lazy as _

from .models import Posts, UserMessages

from markdownx.fields import MarkdownxFormField
from clean_fields.models import CleanFieldsModel

class PostForm(forms.ModelForm):
	summary = MarkdownxFormField()
	body = MarkdownxFormField()

	class Meta:
		model = Posts
		fields = [
			'title',
			'image',
			'project_url',
		]

class MessageForm(forms.ModelForm):
	class Meta:
		model = UserMessages
		fields = [
			'nickname',
			'subject',
			'body',
			'email',
		]
		labels = {
            'nickname': '',
			'subject': '',
			'body': '',
			'email': '',
        }
		
		widgets = {
			'nickname': forms.TextInput(attrs={'class': 'form-control', 'placeholder': "Nickname (optional)"}),
			'subject': forms.TextInput(attrs={'class': 'form-control', 'placeholder': "Subject"}),
			'body': forms.Textarea(attrs={'class': 'form-control', 'placeholder': "Message"}),
			'email': forms.TextInput(attrs={'class': 'form-control', 'placeholder': "E-Mail (optional)"}),
		}