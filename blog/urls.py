from django.conf.urls import url
from . import views
from django.contrib import admin
from django.contrib.auth import views as auth_views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^post/$', views.post, name='post'),
    url(r'^gallery/$', views.gallery, name='gallery'),
    url(r'^posts/(?P<id>\d+)/$', views.details, name='details'),
    url(r'^about/$',views.about, name='about'),
];
