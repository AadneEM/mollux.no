# Generated by Django 2.0 on 2017-12-14 04:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0003_auto_20171214_0435'),
    ]

    operations = [
        migrations.AddField(
            model_name='posts',
            name='image_path',
            field=models.FilePathField(blank=True, null=True),
        ),
    ]
