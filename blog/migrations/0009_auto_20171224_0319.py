# Generated by Django 2.0 on 2017-12-24 02:19

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0008_auto_20171222_2120'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='posts',
            name='image_path',
        ),
        migrations.AddField(
            model_name='posts',
            name='height_field',
            field=models.IntegerField(default=0),
        ),
        migrations.AddField(
            model_name='posts',
            name='width_field',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='posts',
            name='image',
            field=models.ImageField(blank=True, height_field='height_field', null=True, upload_to='upload_location', width_field='width_field'),
        ),
    ]
