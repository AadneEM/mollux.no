# Generated by Django 2.0 on 2017-12-22 20:14

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('blog', '0006_auto_20171222_2107'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='posts',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='posts',
            name='image',
        ),
        migrations.RemoveField(
            model_name='posts',
            name='image_path',
        ),
        migrations.RemoveField(
            model_name='posts',
            name='project_url',
        ),
    ]
