from django.db import models
from datetime import datetime
from django.urls import reverse

from easy_thumbnails.signals import saved_file
from easy_thumbnails.signal_handlers import generate_aliases_global

from markdownx.models import MarkdownxField
from markdownx.utils import markdownify

def upload_location(instance, filename):
    return "%s/%s" %(instance.id, filename)

class Posts(models.Model):
    title = models.CharField(max_length=200)
    body = MarkdownxField()
    summary = MarkdownxField()
    includeSummary = models.BooleanField(default=True)
    image = models.ImageField(
        null=True,
        blank=True,
        upload_to=upload_location,
        height_field="height_field",
        width_field="width_field"
    )
    height_field = models.IntegerField(default=0, null=True, blank=True,)
    width_field = models.IntegerField(default=0, null=True, blank=True,)
    project_url = models.URLField(null=True, blank=True)
    created_at = models.DateTimeField(null=True, default=datetime.now, blank=True)
    unity_project = models.BooleanField(default=False)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Posts"

    @property
    def formatted_summary(self):
        return markdownify(self.summary)

    @property
    def formatted_body(self):
        return markdownify(self.body)

    def get_absolute_url(self):
        return reverse("details", kwargs={"id": self.id})

class SidebarElements(models.Model):
    title = models.CharField(max_length=200)
    body = MarkdownxField()
    priority = models.IntegerField(default=0)

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = "Sidebar Elements"

class UserMessages(models.Model):
    nickname = models.CharField(max_length=150, default='Anonymous', null=True, blank=True) 
    subject = models.CharField(max_length=150)
    email = models.EmailField(max_length=70, null=True, blank=True)
    body = models.TextField()
    submitted = models.DateTimeField(null=True, default=datetime.now, blank=True)

    def __str__(self):
        if self.nickname:
            return self.nickname + " : " + self.subject
        else:
            return self.subject
    
    class Meta:
        verbose_name_plural = "User Messages"


saved_file.connect(generate_aliases_global)
