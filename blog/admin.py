from django.contrib import admin

# Register your models here.
from .models import Posts
from .models import SidebarElements
from .models import UserMessages


from markdownx.admin import MarkdownxModelAdmin

admin.site.register(Posts, MarkdownxModelAdmin)
admin.site.register(SidebarElements, MarkdownxModelAdmin)
admin.site.register(UserMessages)
